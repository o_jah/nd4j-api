package org.nd4j.linalg.api.shape.loop.three;

import java.io.Serializable;

import org.nd4j.linalg.api.buffer.DataBuffer;
import org.nd4j.linalg.api.shape.Shape;
import org.nd4j.linalg.api.shape.StridePermutation;
import org.nd4j.linalg.util.ArrayUtil;

/**
 * Raw array iteration information Used in preparing for linear array raw
 * iteration
 *
 * @author Adam Gibson
 */
public class RawArrayIterationInformation3 implements Serializable {
	private int nDim;
	private int aOffset = -1;
	private int bOffset = -1;
	private int cOffset = 01;
	private int[] aStrides;
	private int[] bStrides;
	private int[] cStrides;
	private int[] shape;
	private DataBuffer a, b, c;

	public RawArrayIterationInformation3() {

	}

	public int getnDim() {
		return nDim;
	}

	public int getAOffset() {
		return aOffset;
	}

	public int getBOffset() {
		return bOffset;
	}

	public int getCOffset() {
		return cOffset;
	}

	public int[] getAStrides() {
		return aStrides;
	}

	public int[] getBStrides() {
		return bStrides;
	}

	public int[] getCStrides() {
		return cStrides;
	}

	public int[] getShape() {
		return shape;
	}

	public DataBuffer getA() {
		return a;
	}

	public DataBuffer getB() {
		return b;
	}

	public DataBuffer getC() {
		return c;
	}

	public RawArrayIterationInformation3(int nDim, int aOffset, int bOffset,
			int cOffset, int[] aStrides, int[] bStrides, int[] cStrides,
			int[] shape, DataBuffer a, DataBuffer b, DataBuffer c) {
		super();
		this.nDim = nDim;
		this.aOffset = aOffset;
		this.bOffset = bOffset;
		this.cOffset = cOffset;
		this.aStrides = aStrides;
		this.bStrides = bStrides;
		this.cStrides = cStrides;
		this.shape = shape;
		this.a = a;
		this.b = b;
		this.c = c;
	}

	public RawArrayIterationInformation3 computeOut() {
		int aOffset = this.aOffset;
		int bOffset = this.bOffset;
		int[] aStrides = ArrayUtil.copy(this.aStrides);
		int[] bStrides = ArrayUtil.copy(this.bStrides);
		int[] cStrides = ArrayUtil.combine(this.cStrides);
		int[] shape = ArrayUtil.copy(this.shape);
		int nDim = this.nDim;
		StridePermutation[] perms = Shape.createSortedStrides(aStrides);

		for (int i = 0; i < nDim; i++) {
			int iPerm = perms[nDim - i - 1].getPermutation();
			shape[i] = this.shape[iPerm];
			aStrides[i] = aStrides[iPerm];
			bStrides[i] = bStrides[iPerm];
		}

		for (int i = 0; i < nDim; i++) {
			int outStrideA = aStrides[i];
			int outStrideB = bStrides[i];
			int outStrideC = cStrides[i];
			int shapeI = shape[i];

			if (outStrideA < 0) {
				aOffset += outStrideA * shapeI - 1;
				bOffset += outStrideB * shapeI - 1;
				cOffset += outStrideC * shapeI - 1;

				aStrides[i] -= outStrideA;
				bStrides[i] -= outStrideB;
				cStrides[i] -= outStrideC;
			}
		}

		int i = 0;
		for (int j = 1; j < nDim; j++) {
			if (shape[i] == 1) {
				shape[i] = shape[j];
				aStrides[i] = aStrides[j];
				bStrides[i] = aStrides[j];
				cStrides[i] = cStrides[j];
			} else if (shape[j] == 1) {
				// drops axis j
			} else if (aStrides[i] * shape[i] == aStrides[j]
					&& bStrides[i] * shape[i] == bStrides[j]
					&& cStrides[i] * shape[i] == cStrides[j]) {
				shape[i] *= shape[j];
			}

			else {
				i++;
				shape[i] = shape[j];
				aStrides[i] = aStrides[j];
				bStrides[i] = bStrides[j];
				cStrides[i] = cStrides[j];
			}

		}

		nDim = i + 1;

		return RawArrayIterationInformation3.builder().aOffset(aOffset)
				.bOffset(bOffset).aStrides(aStrides).bStrides(bStrides).a(a)
				.b(b).c(c).shape(shape).nDim(nDim).build();
	}

	private static Builder builder() {
		return new Builder();
	}

	public static class Builder {
		public int aOffset;
		public int nDim;
		public int[] shape;
		public int[] bStrides;
		public int[] aStrides;
		public int bOffset;
		public DataBuffer d;
		public DataBuffer c;
		public DataBuffer b;
		public DataBuffer a;

		public Builder aOffset(int aOffset) {
			this.aOffset = aOffset;
			return this;
		}

		public RawArrayIterationInformation3 build() {
			RawArrayIterationInformation3 instance = new RawArrayIterationInformation3();
			instance.aOffset = aOffset;
			instance.nDim = nDim;
			instance.shape = shape;
			instance.bStrides = bStrides;
			instance.aStrides = aStrides;
			instance.bOffset = bOffset;
			instance.c = c;
			instance.b = b;
			instance.a = a;
			return instance;
		}

		public Builder nDim(int nDim) {
			this.nDim = nDim;
			return this;
		}

		public Builder shape(int[] shape) {
			this.shape = shape;
			return this;
		}

		public Builder bStrides(int[] bStrides) {
			this.bStrides = bStrides;
			return this;
		}

		public Builder aStrides(int[] aStrides) {
			this.aStrides = aStrides;
			return this;
		}

		public Builder bOffset(int bOffset) {
			this.bOffset = bOffset;
			return this;
		}

		public Builder d(DataBuffer d) {
			this.d = d;
			return this;
		}

		public Builder c(DataBuffer c) {
			this.c = c;
			return this;
		}

		public Builder b(DataBuffer b) {
			this.b = b;
			return this;
		}

		public Builder a(DataBuffer a) {
			this.a = a;
			return this;
		}

	}
}
