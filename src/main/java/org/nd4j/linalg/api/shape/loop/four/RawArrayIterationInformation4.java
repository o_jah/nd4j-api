package org.nd4j.linalg.api.shape.loop.four;

import java.io.Serializable;

import org.nd4j.linalg.api.buffer.DataBuffer;
import org.nd4j.linalg.api.shape.Shape;
import org.nd4j.linalg.api.shape.StridePermutation;
import org.nd4j.linalg.util.ArrayUtil;

import lombok.Data;

/**
 * Raw array iteration information
 * Used in preparing
 * for linear array raw
 * iteration
 *
 * @author Adam Gibson
 */
@Data
public  class RawArrayIterationInformation4 implements Serializable {
    private int nDim;
    private int aOffset = -1;
    private int bOffset = -1;
    private int cOffset = -1;
    private int dOffset = -1;
    private int[] aStrides;
    private int[] bStrides;
    private int[] cStrides;
    private int[] dStrides;
    private int[] shape;
    private DataBuffer a,b,c,d;
    
    

    public int getNDim() {
		return nDim;
	}

	public int getAOffset() {
		return aOffset;
	}

	public int getBOffset() {
		return bOffset;
	}

	public int getCOffset() {
		return cOffset;
	}

	public int getDOffset() {
		return dOffset;
	}

	public int[] getAStrides() {
		return aStrides;
	}

	public int[] getBStrides() {
		return bStrides;
	}

	public int[] getCStrides() {
		return cStrides;
	}

	public int[] getDStrides() {
		return dStrides;
	}

	public int[] getShape() {
		return shape;
	}

	public DataBuffer getA() {
		return a;
	}

	public DataBuffer getB() {
		return b;
	}

	public DataBuffer getC() {
		return c;
	}

	public DataBuffer getD() {
		return d;
	}

	public RawArrayIterationInformation4 computeOut() {
        int aOffset = this.aOffset;
        int bOffset = this.bOffset;
        int[] aStrides = ArrayUtil.copy(this.aStrides);
        int[] bStrides = ArrayUtil.copy(this.bStrides);
        int[] cStrides = ArrayUtil.copy(this.cStrides);
        int[] dStrides = ArrayUtil.copy(this.dStrides);
        int[] shape = ArrayUtil.copy(this.shape);
        int nDim = this.nDim;
        StridePermutation[] perms = Shape.createSortedStrides(aStrides);


        for(int i = 0; i < nDim; i++) {
            int iPerm = perms[nDim - i - 1].getPermutation();
            shape[i] = this.shape[iPerm];
            aStrides[i] = aStrides[iPerm];
            bStrides[i] = bStrides[iPerm];
            cStrides[i] = cStrides[iPerm];
            dStrides[i] = dStrides[iPerm];

        }

        for(int i = 0; i < nDim; i++) {
            int outStrideA = aStrides[i];
            int outStrideB = bStrides[i];
            int outStrideC = cStrides[i];
            int outStrideD = dStrides[i];

            int shapeI = shape[i];

            if(outStrideA < 0) {
                aOffset += outStrideA * shapeI - 1;
                bOffset += outStrideB * shapeI - 1;
                aStrides[i] -= outStrideA;
                bStrides[i] -= outStrideB;
                cStrides[i] -= outStrideC;
                dStrides[i] -= outStrideD;
            }
        }

        int i = 0;
        for(int j = 1; j < nDim; j++) {
            if(shape[i] == 1) {
                shape[i] = shape[j];
                aStrides[i] =  aStrides[j];
                bStrides[i] = bStrides[j];
                cStrides[i] = cStrides[j];
                dStrides[i] = dStrides[j];

            }
            else if(shape[j] == 1) {
                //drops axis j
            }
            else if(aStrides[i] * shape[i] == aStrides[j] && bStrides[i] * shape[i] == bStrides[j] && bStrides[i] * shape[i] == cStrides[j] && dStrides[i] * shape[i] == dStrides[j]) {
                shape[i] *= shape[j];
            }

            else {
                i++;
                shape[i] = shape[j];
                aStrides[i] = aStrides[j];
                bStrides[i] = bStrides[j];
                cStrides[i] = cStrides[j];
                dStrides[i] = dStrides[j];

            }


        }

        nDim = i + 1;

        return  RawArrayIterationInformation4.builder().aOffset(aOffset)
                .a(a).b(b).c(c).d(d)
                .bOffset(bOffset).aStrides(aStrides).bStrides(bStrides)
                .shape(shape).nDim(nDim).build();
    }
    
    private static Builder builder() {
		return new Builder();
	}

	public static class Builder {

		public int aOffset;
		public int nDim;
		public int[] shape;
		public int[] bStrides;
		public int[] aStrides;
		public int bOffset;
		public DataBuffer d;
		public DataBuffer c;
		public DataBuffer b;
		public DataBuffer a;

		public Builder aOffset(int aOffset) {
			this.aOffset = aOffset;
			return this;
		}

		public RawArrayIterationInformation4 build() {
			RawArrayIterationInformation4 instance = new RawArrayIterationInformation4();
			instance.aOffset = aOffset;
			instance.nDim = nDim;
			instance.shape = shape;
			instance.bStrides = bStrides;
			instance.aStrides = aStrides;
			instance.bOffset = bOffset;
			instance.d = d;
			instance.c = c;
			instance.b = b;
			instance.a = a;
			return instance;
		}

		public Builder nDim(int nDim) {
			this.nDim = nDim;
			return this;
		}

		public Builder shape(int[] shape) {
			this.shape = shape;
			return this;
		}

		public Builder bStrides(int[] bStrides) {
			this.bStrides = bStrides;
			return this;
		}

		public Builder aStrides(int[] aStrides) {
			this.aStrides = aStrides;
			return this;
		}

		public Builder bOffset(int bOffset) {
			this.bOffset = bOffset;
			return this;
		}

		public Builder d(DataBuffer d) {
			this.d = d;
			return this;
		}

		public Builder c(DataBuffer c) {
			this.c = c;
			return this;
		}

		public Builder b(DataBuffer b) {
			this.b = b;
			return this;
		}

		public Builder a(DataBuffer a) {
			this.a = a;
			return this;
		}

	}
}
