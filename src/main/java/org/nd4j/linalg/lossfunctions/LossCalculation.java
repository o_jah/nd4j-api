package org.nd4j.linalg.lossfunctions;

import static org.nd4j.linalg.ops.transforms.Transforms.log;
import static org.nd4j.linalg.ops.transforms.Transforms.pow;
import static org.nd4j.linalg.ops.transforms.Transforms.sqrt;

import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.indexing.BooleanIndexing;
import org.nd4j.linalg.indexing.conditions.Conditions;
import org.nd4j.linalg.indexing.conditions.Or;
import org.nd4j.linalg.indexing.functions.StableNumber;
import org.nd4j.linalg.indexing.functions.Value;

/**
 * @author Adam Gibson
 */
public class LossCalculation {
	private INDArray labels;
	private INDArray z;
	/**
	 * L1/L2 values: before division by miniBatchSize, but after multiplication
	 * by l1Coeff or 0.5*l2Coeff
	 */
	private double l1, l2;
	private LossFunctions.LossFunction lossFunction;
	private boolean useRegularization;
	private boolean miniBatch = false;
	private int miniBatchSize;

	public INDArray getLabels() {
		return labels;
	}

	public INDArray getZ() {
		return z;
	}

	public double getL1() {
		return l1;
	}

	public double getL2() {
		return l2;
	}

	public LossFunctions.LossFunction getLossFunction() {
		return lossFunction;
	}

	public boolean isUseRegularization() {
		return useRegularization;
	}

	public boolean isMiniBatch() {
		return miniBatch;
	}

	public int getMiniBatchSize() {
		return miniBatchSize;
	}

	public static LossCalculationBuilder builder() {
		return new LossCalculationBuilder();
	}

	public static class LossCalculationBuilder {
		private INDArray labels;
		private INDArray z;
		private double l1, l2;
		private LossFunctions.LossFunction lossFunction;
		private boolean useRegularization;
		private boolean miniBatch;
		private int miniBatchSize;

		public LossCalculationBuilder l1(double l1) {
			this.l1 = l1;
			return this;
		}
		
		public LossCalculationBuilder miniBatch(boolean miniBatch) {
			this.miniBatch = miniBatch;
			return this;
		}
		
		public LossCalculationBuilder miniBatchSize(int miniBatchSize) {
			this.miniBatchSize = miniBatchSize;
			return this;
		}

		public LossCalculationBuilder labels(INDArray labels) {
			this.labels = labels;
			return this;
		}

		public LossCalculationBuilder z(INDArray z) {
			this.z = z;
			return this;
		}

		public LossCalculationBuilder l2(double l2) {
			this.l2 = l2;
			return this;
		}

		public LossCalculationBuilder lossFunction(
				LossFunctions.LossFunction lossFunction) {
			this.lossFunction = lossFunction;
			return this;
		}

		public LossCalculationBuilder useRegularization(
				boolean useRegularization) {
			this.useRegularization = useRegularization;
			return this;
		}

		public LossCalculation build() {
			LossCalculation lossCalculation = new LossCalculation();
			lossCalculation.l1 = l1;
			lossCalculation.l2 = l2;

			lossCalculation.labels = labels;
			lossCalculation.lossFunction = lossFunction;
			lossCalculation.useRegularization = useRegularization;
			lossCalculation.miniBatch = miniBatch;
			lossCalculation.miniBatchSize = miniBatchSize;
			lossCalculation.z = z;
			return lossCalculation;
		}
	}

	public double score() {
		double ret = 0.0;
		switch (lossFunction) {
		case CUSTOM:
			throw new IllegalStateException(
					"Unable to score custom operation. Please define an alternative mechanism");
		case RECONSTRUCTION_CROSSENTROPY:
			INDArray xEntLogZ2 = logZ(z);
			INDArray xEntOneMinusLabelsOut2 = labels.rsub(1);
			INDArray xEntOneMinusLogOneMinusZ2 = xEntLogZ2.rsubi(1);
			ret = -labels.mul(xEntLogZ2).add(xEntOneMinusLabelsOut2)
					.muli(xEntOneMinusLogOneMinusZ2).sumNumber().doubleValue();
			break;
		case MCXENT:
			INDArray sums = labels.mul(logZ(z));
			ret = -sums.sumNumber().doubleValue();
			break;
		case XENT:
			INDArray xEntLogZ = logZ(z);
			INDArray xEntOneMinusLabelsOut = labels.rsub(1);
			INDArray xEntOneMinusLogOneMinusZ = xEntLogZ.dup().rsubi(1);
			ret = labels.mul(xEntLogZ).add(xEntOneMinusLabelsOut)
					.muli(xEntOneMinusLogOneMinusZ).sum(1).sumNumber()
					.doubleValue();
			break;
		case RMSE_XENT:
			INDArray rmseXentDiff = labels.sub(z);
			INDArray squaredrmseXentDiff = pow(rmseXentDiff, 2.0);
			INDArray sqrt = sqrt(squaredrmseXentDiff);
			ret = sqrt.sumNumber().doubleValue();
			break;
		case MSE:
			INDArray mseDelta = labels.sub(z);
			ret = 0.5 * pow(mseDelta, 2).sum(1).sumNumber().doubleValue();
			break;
		case EXPLL:
			INDArray expLLLogZ = logZ(z);
			ret = z.sub(labels.mul(expLLLogZ)).sumNumber().doubleValue();
			break;
		case SQUARED_LOSS:
			ret = pow(labels.sub(z), 2).sumNumber().doubleValue();
			break;
		case NEGATIVELOGLIKELIHOOD:
			INDArray log = logZ(z);
			INDArray sums2 = labels.mul(log);
			ret = -sums2.sumNumber().doubleValue();
			break;
		}

		if (useRegularization) {
			ret += l1 + l2;
		}

		if (miniBatch)
			ret /= (double) miniBatchSize;
		return ret;
	}

	private static INDArray logZ(INDArray z) {
		INDArray log = log(z, true);

		// log approaches -Infinity as z approaches zero. Replace -Infinity with
		// the least possible value.
		// Caveat: does not handle +Infinity since z is assumed to be 0 <= z <=
		// 1.
		switch (log.data().dataType()) {
		case FLOAT:
			BooleanIndexing.applyWhere(log,
					new Or(Conditions.isNan(), Conditions.isInfinite()),
					new StableNumber(StableNumber.Type.FLOAT));
			break;
		case DOUBLE:
			BooleanIndexing.applyWhere(log,
					new Or(Conditions.isNan(), Conditions.isInfinite()),
					new StableNumber(StableNumber.Type.DOUBLE));

			break;
		case INT:
			BooleanIndexing.applyWhere(log,
					new Or(Conditions.isNan(), Conditions.isInfinite()),
					new Value(-Integer.MAX_VALUE));
			break;
		default:
			throw new RuntimeException(
					"unsupported data type: " + log.data().dataType());
		}
		return log;
	}

}
