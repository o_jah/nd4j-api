package org.nd4j.linalg.api.blas.params;

import org.nd4j.linalg.api.complex.IComplexNDArray;
import org.nd4j.linalg.api.ndarray.INDArray;

/**
 * Gemv parameters: The parameters for general matrix vector operations
 *
 * @author Adam Gibson
 */
public class GemvParameters {
	private int m, n, lda, incx, incy;
	private INDArray a, x, y;
	private char aOrdering = 'N';

	public int getM() {
		return m;
	}

	public int getN() {
		return n;
	}

	public int getLda() {
		return lda;
	}

	public int getIncx() {
		return incx;
	}

	public int getIncy() {
		return incy;
	}

	public INDArray getA() {
		return a;
	}

	public INDArray getX() {
		return x;
	}

	public INDArray getY() {
		return y;
	}

	public char getAOrdering() {
		return aOrdering;
	}

	public GemvParameters(INDArray a, INDArray x, INDArray y) {

		this.a = a;
		this.x = x;
		this.y = y;

		if (a.ordering() == 'f' && a.isMatrix()) {
			this.m = a.rows();
			this.n = a.columns();
			this.lda = a.rows();
		} else if (a.ordering() == 'c' && a.isMatrix()) {
			this.m = a.columns();
			this.n = a.rows();
			this.lda = a.columns();
			aOrdering = 'T';
		}

		else {
			this.m = a.rows();
			this.n = a.columns();
			this.lda = a.size(0);
		}

		if (x.isColumnVector()) {
			if (x.ordering() == 'f')
				incx = x.stride(0);
			else
				incx = x.stride(1);
		} else {
			if (x.ordering() == 'f')
				incx = x.stride(1);
			else
				incx = x.stride(0);
		}

		this.incy = y.elementStride();

		if (x instanceof IComplexNDArray)
			this.incx /= 2;
		if (y instanceof IComplexNDArray)
			this.incy /= 2;

	}

}
