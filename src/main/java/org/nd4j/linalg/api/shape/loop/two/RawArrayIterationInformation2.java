package org.nd4j.linalg.api.shape.loop.two;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.nd4j.linalg.api.buffer.DataBuffer;
import org.nd4j.linalg.api.shape.Shape;
import org.nd4j.linalg.api.shape.StridePermutation;
import org.nd4j.linalg.util.ArrayUtil;

import java.io.Serializable;

/**
 * Raw array iteration information Used in preparing for linear array raw
 * iteration
 *
 * @author Adam Gibson
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class RawArrayIterationInformation2 implements Serializable {
	private int nDim;
	private int aOffset = -1;
	private int bOffset = -1;
	private int[] aStrides;
	private int[] bStrides;
	private int[] shape;
	private DataBuffer a;
	private DataBuffer b;

	public int getNDim() {
		return nDim;
	}

	public void setNDim(int nDim) {
		this.nDim = nDim;
	}

	public int getAOffset() {
		return aOffset;
	}

	public void setAOffset(int aOffset) {
		this.aOffset = aOffset;
	}

	public int getBOffset() {
		return bOffset;
	}

	public void setBOffset(int bOffset) {
		this.bOffset = bOffset;
	}

	public int[] getAStrides() {
		return aStrides;
	}

	public void setAStrides(int[] aStrides) {
		this.aStrides = aStrides;
	}

	public int[] getBStrides() {
		return bStrides;
	}

	public void setBStrides(int[] bStrides) {
		this.bStrides = bStrides;
	}

	public int[] getShape() {
		return shape;
	}

	public void setShape(int[] shape) {
		this.shape = shape;
	}

	public DataBuffer getA() {
		return a;
	}

	public void setA(DataBuffer a) {
		this.a = a;
	}

	public DataBuffer getB() {
		return b;
	}

	public void setB(DataBuffer b) {
		this.b = b;
	}

	/**
	 * Resolve the new strides/shapes
	 * 
	 * @return
	 */
	public RawArrayIterationInformation2 computeOut() {
		int aOffset = this.aOffset;
		int bOffset = this.bOffset;
		int[] aStrides = ArrayUtil.copy(this.aStrides);
		int[] bStrides = ArrayUtil.copy(this.bStrides);
		int[] shape = ArrayUtil.copy(this.shape);
		int nDim = this.nDim;
		StridePermutation[] perms = Shape.createSortedStrides(aStrides);

		for (int i = 0; i < nDim; i++) {
			int iPerm = perms[nDim - i - 1].getPermutation();
			shape[i] = this.shape[iPerm];
			aStrides[i] = aStrides[iPerm];
			bStrides[i] = bStrides[iPerm];
		}

		for (int i = 0; i < nDim; i++) {
			int outStrideA = aStrides[i];
			int outStrideB = bStrides[i];
			int shapeI = shape[i];

			if (outStrideA < 0) {
				aOffset += outStrideA * shapeI - 1;
				bOffset += outStrideB * shapeI - 1;
				aStrides[i] -= outStrideA;
				bStrides[i] -= outStrideB;
			}
		}

		int i = 0;
		for (int j = 1; j < nDim; j++) {
			if (shape[i] == 1) {
				shape[i] = shape[j];
				aStrides[i] = aStrides[j];
				bStrides[i] = aStrides[j];
			} else if (shape[j] == 1) {
				// drops axis j
			} else if (aStrides[i] * shape[i] == aStrides[j]
					&& bStrides[i] * shape[i] == bStrides[j]) {
				shape[i] *= shape[j];
			}

			else {
				i++;
				shape[i] = shape[j];
				aStrides[i] = aStrides[j];
				bStrides[i] = bStrides[j];
			}

		}

		nDim = i + 1;
		// need to force vectors and scalars to be 2d
		if (nDim == 1) {
			nDim = 2;
			shape = this.shape;
			// reset
			aStrides = this.aStrides;
			bStrides = this.bStrides;
		}

		return RawArrayIterationInformation2.builder().aOffset(aOffset).a(a)
				.b(b).bOffset(bOffset).aStrides(aStrides).bStrides(bStrides)
				.shape(shape).nDim(nDim).build();
	}

	public static Builder builder() {
		// TODO Auto-generated method stub
		return null;
	}

	public static class Builder {

		public int aOffset;
		public int nDim;
		public int[] shape;
		public int[] bStrides;
		public int[] aStrides;
		public int bOffset;
		public DataBuffer d;
		public DataBuffer c;
		public DataBuffer b;
		public DataBuffer a;

		public Builder nDim(int nDim) {
			this.nDim = nDim;
			return this;
		}

		public RawArrayIterationInformation2 build() {
			RawArrayIterationInformation2 instance = new RawArrayIterationInformation2();
			instance.aOffset = aOffset;
			instance.nDim = nDim;
			instance.shape = shape;
			instance.bStrides = bStrides;
			instance.aStrides = aStrides;
			instance.bOffset = bOffset;
			instance.b = b;
			instance.a = a;
			return instance;
		}

		public Builder aOffset(int aOffset) {
			this.aOffset = aOffset;
			return this;
		}

		public Builder shape(int[] shape) {
			this.shape = shape;
			return this;
		}

		public Builder bStrides(int[] bStrides) {
			this.bStrides = bStrides;
			return this;
		}

		public Builder aStrides(int[] aStrides) {
			this.aStrides = aStrides;
			return this;
		}

		public Builder bOffset(int bOffset) {
			this.bOffset = bOffset;
			return this;
		}

		public Builder d(DataBuffer d) {
			this.d = d;
			return this;
		}

		public Builder c(DataBuffer c) {
			this.c = c;
			return this;
		}

		public Builder b(DataBuffer b) {
			this.b = b;
			return this;
		}

		public Builder a(DataBuffer a) {
			this.a = a;
			return this;
		}

	}
}
